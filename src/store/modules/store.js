import api from '@/api';

export default {
    state: {
        products: [],
        shopping_cart: []
    },
    mutations: {
        setProducts(state, products){
            state.products = products;
        },
        changeProduct(state, article){
            state.products[article.id].title = article.products.title;
            state.products[article.id].text = article.products.text;
            state.products[article.id].img = article.products.img;
            state.products[article.id].cost = article.products.cost;
        },
        deleteProduct(state, id){
            state.products.splice(id, 1);
        },
        setCart(state, article){
            state.shopping_cart.push(article);
        },
        daleteCart(state, id){
            state.shopping_cart.splice(id, 1);
        }
    },
    actions: {
        addProduct({state, commit}, article){
            api.axios.put(api.urls.store, article).then(res => {
                let newProducts = state.products.concat();
                newProducts.push(res.data);
                commit('setProducts', newProducts);
            })
        },
        getProducts({state, commit}){
            api.axios.get(api.urls.store).then(res => {
                commit('setProducts', res.data);
            })
        },
        changeProduct({state, commit}, article){
            api.axios.post(api.urls.store, article).then(res => {
                if (res.data == 'ok') commit('changeProduct', article);
                else console.log('Error!');
            })
        },
        deleteProduct({state, commit}, art){
            api.axios.delete(api.urls.store + '/' + art._id).then(res => {
                if (res.data == 'ok') commit('deleteProduct', art.id);
                else console.log('Error!');
            })
        },
        add_item_to_cart({state, commit}, article){
            commit('setCart', article);
        },
        delete_item_to_cart({state, commit}, id){
            commit('daleteCart', id);
        }
    }
}