import api from '@/api';

export default {
    state: {
        users: [],
        user: {}
    },
    mutations: {
        setUser(state, user){state.user = user},
        setUsers(state, users){state.users = users}
    },
    actions: {
        getUsers({state, commit}){
            api.axios.get(api.urls.users + '/all').then(res => {
                commit('setUsers', res.data);
            })
        },
        getOneUser({state, commit}){
            api.axios.get(api.urls.users + '/one').then(res => {
                commit('setUser', res.data);
            })
        },
        addUser({state, commit}, article){
            return api.axios.post(api.urls.users + '/add', article).then(res => {
                if (res.data != 'ok'){
                    alert(res.data)
                    return false
                }
                else return true
            })
        },
        changeUser({state, commit}, article){
            api.axios.post(api.urls.users + '/change', article).then(res => {
                if (res.data == 'Error!') alert("Error!")
                else commit('setUser', article)
            })
        },
        login({state, commit}, article){
            api.axios.post(api.urls.login, article).then(res => {
                localStorage.setItem('jwt', res.data)
                window.location.href = "http://192.168.1.118:8080/#/account"
                window.location.reload()
            })
        }
    }
}