import api from '@/api/index'

export default {
    state: {
        chats: []
    },
    mutations: {
        setChat(state, chat){
            state.chats = chat
        },
        addMessage(state, article){
            state.chats[article.id].messages.push(article.message)
        },
        changeChat(state, article){
            state.chats[article.id].title = article.chat.title;
            state.chats[article.id].admin = article.chat.admin;
            state.chats[article.id].users = article.chat.users;
            state.chats[article.id].messages = article.chat.messages;
        },
        deleteChat(state, id){
            state.chats.splice(id, 1);
        }
    },
    actions: {
        getChats({state, commit}){
            api.axios.get(api.urls.chats).then(res => {
                commit('setChat', res.data);
            })
        },
        getChat({state, commit}, id){
            api.axios.post(api.urls.chats + '/get', id).then(res => {
                commit('setOneChat', res.data);
            })
        },
        addChat({state, commit}, article){
            api.axios.put(api.urls.chats, article).then(res => {
                if (res.data == 'ok'){
                    let newChat = state.chats.concat();
                    newChat.push(article);
                    commit('setChat', newChat);
                }
                else console.log('Error!')
            })
        },
        changeChat({state, commit}, article){
            api.axios.post(api.urls.chats + '/change', article.chat).then(res => {
                commit('changeChat', article);
            })
        },
        deleteChat({state, commit}, article){
            api.axios.delete(api.urls.chats + '/' + article._id).then(res => {
                commit('deleteChat', article.id);
            })
        },
        addMessage({state, commit}, article){
            api.axios.post(api.urls.chats + '/message', article).then(res => {
                if (res.data == 'ok') commit('addMessage', article);
                else console.log('Error!')
            })
        }
    }
}