import api from '@/api';

export default {
    state: {
        news: []
    },
    mutations: {
        setNews(state, news){
            state.news = news;
        },
        changeNews(state, article){
            state.news[article.id].title = article.news.title;
            state.news[article.id].text = article.news.text;
            state.news[article.id].img = article.news.img;
            state.news[article.id].like = article.news.like;
            state.news[article.id].comment = article.news.comment;
            state.news[article.id].share = article.news.share;
        },
        deleteNews(state, id){
            state.news.splice(id, 1);
        }
    },
    actions: {
        addNews({state, commit}, article){
            api.axios.put(api.urls.news, article).then(res => {
                let newNews = state.news.concat();
                newNews.unshift(article);
                commit('setNews', newNews);
            })
        },
        getNews({state, commit}){
            api.axios.get(api.urls.news).then(res => {
                commit('setNews', res.data);
            })
        },
        changeNews({state, commit}, article){
            delete article.news.__v
            api.axios.post(api.urls.news, article).then(res => {
                commit('changeNews', article);
            })
        },
        deleteNews({state, commit}, article){
            api.axios.delete(api.urls.news + '/' + article._id).then(res => {
                commit('deleteNews', article.id);
            })
        }
    }
}